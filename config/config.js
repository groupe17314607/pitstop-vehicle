const fs = require('fs');

module.exports = {
  development: {
    username: 'root',
    password: null,
    database: 'database_development',
    host: "vehicle-db",
    dialect: 'mysql',
    logging: false,
    dialectOptions: {
      bigNumberStrings: true
    }
    },
    test: {
      username: 'root',
      password: null,
      database: 'database_test',
      host: 'vehicle-db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true
      }
    },
    production: {
      username: 'root',
      password: "mouilly",
      database: 'database_production',
      host: 'vehicle-db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true,
      }
    }
  };